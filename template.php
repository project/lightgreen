<?php

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function medsavailable_container($L, $R) {
	$class = 'container';
	if ($L) {
		$class .= ' container-left';
	}
	if ($R) {
		$class .= ' container-right';
	}
	
	return ' class="' . $class . '"';
}

function medsavailable_footer($message) {
	return $message;
}

if (arg ( 0 ) == 'node' and is_numeric ( arg ( 1 ) )) {
	$n = node_load ( arg ( 1 ) );
	if ($n->type == 'article') {
		drupal_add_css ( path_to_theme () . '/summary.css' );
	}
}

