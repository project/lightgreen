<?php 
?>
  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  
    <?php if ($picture) {
      print $picture;
    }?>
    
    <?php if (!$page) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    <?php if ($submitted && $page) { ?><span class="submitted"><?php print $submitted; ?></span><?php } ?>
    <?php if ($terms && $page) { ?><span class="taxonomy"><?php print $terms?></span><?php } ?>
    <div class="content"><?php print $content?></div>
    <?php if ($links && $page) { ?><div class="links"><?php print $links?></div><?php } ?>
    
  </div>
