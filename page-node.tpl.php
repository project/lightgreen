<?php 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $title ?></title>
<?php print $head ?><?php print $styles ?>

<?php print $scripts ?>

<!--[if lte IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie-fix.css";</style>
<![endif]-->
<!--[if lte IE 6]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie6-fix.css";</style>
<![endif]-->

</head>
<body>
<div id="wrapper"> <?php print $header; ?>
  <div id="header">
    <?php if ($logo) { ?><div id="logo">
      
      <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><h1><?php if ($site_name) {print $site_name; }?></h1></a>
     
    </div> <?php } ?>
    
	<div class="search_box">
	<?php print $search_box; ?> 
    </div>
    
    <div class="advertisement">
	<?php print $advertisement; ?>
    </div>
    
    <?php if ($site_name) { ?>
    <h1 class="site-name"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1>
    <?php } ?>
  </div>
  <div id="sub_header"> 
    <?php if (isset($primary_links)) { ?>
      <div id="primary">
	      <?php print theme('links', $primary_links, array('class' =>'links nice-menu nice-menu-down', 'id' => 'nice-menu-primary')) ?>
	  </div>      
      <?php } ?>
  <?php print $sub_header ?> 
  </div>

  <div id="container" <?php print lightgreen_container($sidebar_left, $sidebar_right); ?>>
    <div id="center" class="column">
      <div id="main"><!-- ?php print $breadcrumb ? -->
        <?php  if ($content_top) { print $content_top; }?>
		<?php  if ($floater_left || $floater_right) { ?>
         <div id="floater_left"><?php  print $floater_left; ?></div>
         <div id="floater_right"><?php  print $floater_right; ?></div>
    	<?php }?>
		<?php print $top_content; ?> 
		<?php if ($title && !$is_front) {  ?>
        <h1 class="title"><?php print $title ?></h1>
        <?php } ?>
        <?php print $help ?> <?php print $messages ?> 
        <?php if ($tabs) { ?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php } ?>
		<?php print $content; ?><?php print $feed_icons; ?></div>
    </div>
    <?php  if ($sidebar_left) { 	?>
    <div id="left" class="column">
      <?php  print $sidebar_left;  ?>
    </div>
    <?php } ?>
    <?php if ($sidebar_right) { ?>
    <div id="right" class="column">
      <?php   print $sidebar_right; ?>
    </div>
    <?php } ?>
    <div class="clear: both; height: 0px;" ></div>
  </div>
  <div id="footer"><?php print theme('links', $secondary_links); print medsavailable_footer($footer_message); ?></div>
  <div style="width: 100px; float: right; font-size: 80%; text-align: right;">
  <!-- Please Keep this link and Do not remove it, thank you -->
  	Design by <a href="http://ekikrat.in" style="color: gray;">Ekikrat</a>
  	</div>
  <?php print $closure ?>
</div>
</body>
</html>
